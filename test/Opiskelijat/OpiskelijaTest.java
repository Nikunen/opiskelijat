/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Opiskelijat;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author nikue
 */
public class OpiskelijaTest {

    public OpiskelijaTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    /**
     * Test of getOpiskelijanumero method, of class Opiskelija.
     */
    @Test
    public void testGetOpiskelijanumero() {
        System.out.println("getOpiskelijanumero");
        Opiskelija instance = new Opiskelija();
        String expResult = "0";
        String result = instance.getOpiskelijanumero();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setOpiskelijanumero method, of class Opiskelija.
     */
    @Test
    public void testSetOpiskelijanumero() {
        System.out.println("setOpiskelijanumero");
        String opiskelijanumero = "007";
        Opiskelija instance = new Opiskelija();
        instance.setOpiskelijanumero(opiskelijanumero);
        
    }

    /**
     * Test of getEtunimi method, of class Opiskelija.
     */
    @Test
    public void testGetEtunimi() {
        System.out.println("getEtunimi");
        Opiskelija instance = new Opiskelija();
        String expResult = "Testi";
        String result = instance.getEtunimi();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setEtunimi method, of class Opiskelija.
     */
    @Test
    public void testSetEtunimi() {
        System.out.println("setEtunimi");
        String etunimi = "Liisa";
        Opiskelija instance = new Opiskelija();
        instance.setEtunimi(etunimi);
       
    }

    /**
     * Test of getSukunimi method, of class Opiskelija.
     */
    @Test
    public void testGetSukunimi() {
        System.out.println("getSukunimi");
        Opiskelija instance = new Opiskelija();
        String expResult = "Testi";
        String result = instance.getSukunimi();
        assertEquals(expResult, result);
      
    }

    /**
     * Test of setSukunimi method, of class Opiskelija.
     */
    @Test
    public void testSetSukunimi() {
        System.out.println("setSukunimi");
        String sukunimi = "Opiskelija";
        Opiskelija instance = new Opiskelija();
        instance.setSukunimi(sukunimi);
    }

    /**
     * Test of toString method, of class Opiskelija.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Opiskelija instance = new Opiskelija("008", "Rowan","Atkinson");
        String expResult = "Olen opiskelija Rowan Atkinson ja opiskelijanumroni on 008";
        String result = instance.toString();
        assertEquals(expResult, result);
       
    }

}