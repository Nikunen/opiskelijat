/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Opiskelijat;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author nikue
 */
public class OpiskelijaryhmaTest {

    public OpiskelijaryhmaTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    /**
     * Test of getTunnus method, of class Opiskelijaryhmä.
     */
    @Test
    public void testGetTunnus() {
        System.out.println("getTunnus");
        Opiskelijaryhma instance = new Opiskelijaryhma();
        String expResult = "";
        String result = instance.getTunnus();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setTunnus method, of class Opiskelijaryhmä.
     */
    @Test
    public void testSetTunnus() {
        System.out.println("setTunnus");
        String tunnus = "TO10";
        Opiskelijaryhma instance = new Opiskelijaryhma();
        instance.setTunnus(tunnus);
       
    }

    /**
     * Test of getYhteysopiskelija method, of class Opiskelijaryhmä.
     */
    @Test
    public void testGetYhteysopiskelija() {
        System.out.println("getYhteysopiskelija");
        Opiskelijaryhma instance = new Opiskelijaryhma();
        Opiskelija expResult = null;
        Opiskelija result = instance.getYhteysopiskelija();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setYhteysopiskelija method, of class Opiskelijaryhmä.
     */
    @Test
    public void testSetYhteysopiskelija() {
        System.out.println("setYhteysopiskelija");
        Opiskelija yhteysopiskelija = new Opiskelija ("1112304","Uusi","Uljas");
        Opiskelijaryhma instance = new Opiskelijaryhma();
        instance.setYhteysopiskelija(yhteysopiskelija);
        Opiskelija result = instance.getYhteysopiskelija();
       assertEquals(yhteysopiskelija, result);
    }

@Test
public void testLisaaOpiskelija() {
     System.out.println("lisääOpiskelija");
        Opiskelija uopiskelija = new Opiskelija ("1112304","Uusi","Uljas");
        Opiskelijaryhma instance = new Opiskelijaryhma();
        instance.lisaaOpiskelija(uopiskelija);
        System.out.println(instance);
}

}