/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Opiskelijat;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author nikue
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({Opiskelijat.OpiskelijaTest.class,Opiskelijat.OpiskelijaryhmaTest.class})
public class OpiskelijatSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

}