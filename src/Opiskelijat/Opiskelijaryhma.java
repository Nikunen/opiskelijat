/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Opiskelijat;

/**
 *
 * @author nikue
 */
public class Opiskelijaryhma {
private String tunnus;
private Opiskelija yhteysopiskelija;
private int rkoko;
private Opiskelija[] opiskelijat;

public Opiskelijaryhma() {
    opiskelijat = new Opiskelija[50];
    tunnus = "";
    rkoko = 0;
}
    /**
     * @return the tunnus
     */
    public String getTunnus() {
        return tunnus;
    }

    /**
     * @param tunnus the tunnus to set
     */
    public void setTunnus(String tunnus) {
        this.tunnus = tunnus;
    }

    /**
     * @return the yhteysopiskelija
     */
    public Opiskelija getYhteysopiskelija() {
        return yhteysopiskelija;
    }

    /**
     * @param yhteysopiskelija the yhteysopiskelija to set
     */
    public void setYhteysopiskelija(Opiskelija yhteysopiskelija) {
        this.yhteysopiskelija = yhteysopiskelija;
    }

 

    public void lisaaOpiskelija (Opiskelija uOpiskelija) {
        opiskelijat[rkoko] = uOpiskelija;
        rkoko++;
    }
    public String toString() {
        StringBuilder ryhma = new StringBuilder(500);
        for (int i=0; i<rkoko; i++)
            ryhma.append(opiskelijat[i] + "\n");
        return ryhma.toString();
    }
}
