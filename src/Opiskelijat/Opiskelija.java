/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Opiskelijat;

/**
 *
 * @author nikue
 */
public class Opiskelija {
private String opiskelijanumero;
private String etunimi;
private String sukunimi;

public Opiskelija() {
    this("0", "Testi", "Testi");
}

public Opiskelija(String opnro, String enimi, String snimi) {
    opiskelijanumero = opnro;
    etunimi = enimi;
    sukunimi = snimi;
}
    /**
     * @return the opiskelijanumero
     */
    public String getOpiskelijanumero() {
        return opiskelijanumero;
    }

    /**
     * @param opiskelijanumero the opiskelijanumero to set
     */
    public void setOpiskelijanumero(String opiskelijanumero) {
        this.opiskelijanumero = opiskelijanumero;
    }

    /**
     * @return the etunimi
     */
    public String getEtunimi() {
        return etunimi;
    }

    /**
     * @param etunimi the etunimi to set
     */
    public void setEtunimi(String etunimi) {
        this.etunimi = etunimi;
    }

    /**
     * @return the sukunimi
     */
    public String getSukunimi() {
        return sukunimi;
    }

    /**
     * @param sukunimi the sukunimi to set
     */
    public void setSukunimi(String sukunimi) {
        this.sukunimi = sukunimi;
    }

    public String toString() {
        return "Olen opiskelija " + this.etunimi +" " + this.sukunimi + " ja opiskelijanumroni on " + this.opiskelijanumero;
    }
}
